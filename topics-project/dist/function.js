"use strict";
function createPicture(title, date, size) {
    console.log('create Picture using', title, date, size);
}
createPicture('My Birthday', '2020-03-10', '500x500');
createPicture('Colombia Trip', '2020-03');
createPicture('Colombia Trip');
createPicture();
let createPic = (title, date, size) => {
    return { title, date, size };
};
const picture = createPic('Platzi session', '2020-03-10', '100x100');
console.log('picture', picture);
function handleError(code, message) {
    if (message === 'error') {
        throw new Error(`${message}. Code error: ${code}`);
    }
    else {
        return 'An error has occurred';
    }
}
try {
    let result = handleError(200, 'OK');
    console.log('result', result);
    result = handleError(404, 'error');
    console.log('result', result);
}
catch (error) {
}
